﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SingleLevel
{
	public string ID;
	public GameObject AllLevel;
	public GameObject InitPos;
	public GameObject EndingPos;
	public string NextLvlID;
	public void ResetThis(GameObject resetMe)
	{
		resetMe.transform.position = InitPos.transform.position;
	}
}

public class LevelManager : Singleton<LevelManager> 
{
	public List<SingleLevel> AllLevels = new List<SingleLevel>();
	public Dictionary<string,SingleLevel> DicAllLevels = new Dictionary<string, SingleLevel>();
	// Use this for initialization
	void Awake() 
	{
		DontDestroyOnLoad(this.gameObject);
		foreach(SingleLevel data in AllLevels)
		{
			data.AllLevel.SetActive(false);
			if(!DicAllLevels.ContainsKey(data.ID))
			{
				DicAllLevels.Add(data.ID,data);
			}
		}
	}

	public void SetInitPoint(string ID, GameObject resetMe)
	{
		if(DicAllLevels.ContainsKey(ID))
		{
			DicAllLevels[ID].AllLevel.SetActive(true);
			DicAllLevels[ID].ResetThis(resetMe);
		}
	}

	public void SetAllLevel(string ID, bool active)
	{
		if(DicAllLevels.ContainsKey(ID))
		{
			DicAllLevels[ID].AllLevel.SetActive(active);
		}
	}

	public SingleLevel SingleLevelData(string ID)
	{
		if(DicAllLevels.ContainsKey(ID))
		{
			return DicAllLevels[ID];
		}
		else
		{
			return null;
		}
	}
}
