﻿using UnityEngine;
using System.Collections;

public class ActiveScreenButton : MonoBehaviour 
{
	public string ScreenToActive;
	public bool OnlyThis;
	public string NewAudio = string.Empty;
	void OnClick()
	{
		GUIManager.Instance.ActivateScreen(ScreenToActive,OnlyThis);
		if(NewAudio != string.Empty)
		{
			AudioManager.Instance.ActiveAudio(NewAudio);
		}
	}
}
