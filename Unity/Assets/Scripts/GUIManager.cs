﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GUIScreen
{
	public string ID;
	public GameObject[] ArrScreen;
	public void Activate(bool doit)
	{
		foreach(GameObject data in ArrScreen)
		{
			data.SetActive(doit);
		}
	}
}

public class GUIManager : Singleton<GUIManager> 
{
	public List<GUIScreen> AllMyScreens;
	Dictionary<string,GUIScreen> DicScreens = new Dictionary<string, GUIScreen>();
	List<GUIScreen> CurrentScreens = new List<GUIScreen>();
	public string InitScreen;
	public UILabel VidasLabel;
	// Use this for initialization
	void Awake() 
	{
		foreach(GUIScreen data in AllMyScreens)
		{
			if(!DicScreens.ContainsKey(data.ID))
			{
				DicScreens.Add(data.ID,data);
			}
		}
		CurrentScreens = AllMyScreens;
		ActivateScreen(InitScreen,true);
	}
	
	public void ActivateScreen(string id,bool OnlyThis)
	{
		if(DicScreens.ContainsKey(id))
		{
			GUIScreen NewScreen = DicScreens[id];

			foreach(GUIScreen data in CurrentScreens)
			{
				data.Activate(false);
			}
			
			if(OnlyThis)
			{
				CurrentScreens = new List<GUIScreen>();
				CurrentScreens.Add(NewScreen);
			}
			else
			{
				CurrentScreens.Add(NewScreen);
			}

			NewScreen.Activate(true);
		}
	}

	void LateUpdate()
	{
		VidasLabel.text = GameManager.Instance.CurrentVidas.ToString();
	}

	public void Continue()
	{
		GameManager.Instance.Continue();
	}
	
	public void StartNewGame()
	{
		GameManager.Instance.StartNewGame();
	}
}
