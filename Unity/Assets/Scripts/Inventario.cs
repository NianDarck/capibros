﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Item
{
	public string ID;
	public bool PuedeEquipar;
	public int NumeroDe;
}

[System.Serializable]
public class Arma : Item
{
	public float Dano;
	public float Distance;
	public float Uso;
}

[System.Serializable]
public class Usable : Item
{
	public float ModVida;
	public float ModMana;
	public float ModStam;
}

public class Inventario : MonoBehaviour 
{
	public Personaje MiPersonaje;

	public List<Arma> LasArmas = new List<Arma>();
	public List<Usable> LosUsables = new List<Usable>();
	Dictionary<string,Arma> DicArmas = new Dictionary<string, Arma>();
	Dictionary<string,Usable> DicUsables = new Dictionary<string, Usable>();

	public Dictionary<string,int> MisArmas = new Dictionary<string, int>();
	public Dictionary<string,int> MisUsables = new Dictionary<string, int>();

	public string CurrentArma;

	// Use this for initialization
	void Start () 
	{
		if(MiPersonaje != null)
		{
			MiPersonaje.MiInventario = this;
		}

		foreach(Arma data in LasArmas)
		{
			if(!DicArmas.ContainsKey(data.ID))
			{
				DicArmas.Add(data.ID,data);
			}
		}

		foreach(Usable data in LosUsables)
		{
			if(!DicUsables.ContainsKey(data.ID))
			{
				DicUsables.Add(data.ID,data);
			}
		}
	}

	public void AddArma(string id)
	{
		if(!DicArmas.ContainsKey(id))
		{
			return;
		}

		if(!MisArmas.ContainsKey(id))
		{
			MisArmas.Add(id,1);
		}
		else
		{
			int TotalArmas = DicArmas[id].NumeroDe;
			int CurrentArmas = MisArmas[id];
			if(CurrentArmas < TotalArmas)
			{
				CurrentArmas++;
				MisArmas[id] = CurrentArmas;
			}
		}

		Debug.Log(id + ": " + MisArmas[id]);
	}

	public void AddUsable(string id)
	{
		if(!DicUsables.ContainsKey(id))
		{
			return;
		}

		if(!MisUsables.ContainsKey(id))
		{
			MisUsables.Add(id,1);
		}
		else
		{
			int TotalUsables = DicUsables[id].NumeroDe;
			int CurrentUsables = MisUsables[id];
			if(CurrentUsables < TotalUsables)
			{
				CurrentUsables++;
				MisUsables[id] = CurrentUsables;
			}
		}

		Debug.Log(id + ": " + MisUsables[id]);
	}

	public void UseUsable(string id)
	{
		if(MisUsables.ContainsKey(id))
		{
			MisUsables[id]--;

			MiPersonaje.Vida += DicUsables[id].ModVida;
			MiPersonaje.Mana += DicUsables[id].ModMana;
			MiPersonaje.Stamina += DicUsables[id].ModStam;

			int CurrentUsables = MisUsables[id];
			if(CurrentUsables == 0)
			{
				MisUsables.Remove(id);
			}
			Debug.Log(id + ": " + CurrentUsables);
		}
	}

	public void EquipArma(string id)
	{
		if(MisArmas.ContainsKey(id))
		{
			CurrentArma = id;
		}
	}

	public string TempUsableAdd = "Corazon";
	public string TempArmaAdd = "Espada";

	// Update is called once per frame
	void Update() 
	{
		if(Input.GetKeyDown(KeyCode.O))
		{
			AddUsable(TempUsableAdd);
		}

		if(Input.GetKeyDown(KeyCode.I))
		{
			UseUsable(TempUsableAdd);
		}

		if(Input.GetKeyDown(KeyCode.P))
		{
			AddArma(TempArmaAdd);
		}
	}
}
