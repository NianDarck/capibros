﻿using UnityEngine;
using System.Collections;

public class PlataformerInput : MonoBehaviour
{
	#if UNITY_EDITOR
	public string InputHorizontal = "Horizontal";
	public string InputSalto = "Jump";
	public string InputCorrer = "Run";
	public string InputAccion = "Accion";
	#endif

	//public Personaje MiPersonaje;
	public string MessageHorizontal = "InputHorizontal";
	public string MessageSalto = "InputSalto";
	public string MessageCorrer = "InputCorrer";
	public string MessageAccion = "InputAccion";

	#if UNITY_ANDROID || UNITY_IOS
	public float OffsetTouchWalkLeft;
	public float OffsetTouchWalkRight;
	public float TimeTouchRunLeft;
	public float TimeTouchRunRight;
	public Vector2 OffSetScreenMove;
	bool MoveTouchDet = false;
	int MoveTouchID;

	public Vector2 OffSetScreenJump;
	#endif

	void Start()
	{
		/*if(MiPersonaje == null)
		{
			MiPersonaje = gameObject.GetComponent<Personaje>();
		}

		if(MiPersonaje == null)
		{
			Debug.LogError("NO EXISTE PERSONAJE");
		}*/
		Input.multiTouchEnabled = true;
	}

	// Update is called once per frame
	void Update()
    {
		/*if(MiPersonaje == null)
		{
			return;
		}*/
	
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN
		float HorizontalData = Input.GetAxisRaw(InputHorizontal);
	
		SendMessage(MessageHorizontal,HorizontalData,SendMessageOptions.DontRequireReceiver);
		SendMessage(MessageSalto,Input.GetButton(InputSalto),SendMessageOptions.DontRequireReceiver);
		SendMessage(MessageCorrer,Input.GetButton(InputAccion),SendMessageOptions.DontRequireReceiver);
		SendMessage(MessageAccion,Input.GetButton(InputCorrer),SendMessageOptions.DontRequireReceiver);

		/*
		MiPersonaje.InputHorizontal(HorizontalData);
		MiPersonaje.InputCorrer(Input.GetButton(InputCorrer));
		MiPersonaje.InputAccion(Input.GetButton(InputAccion));
		MiPersonaje.InputSalto(Input.GetButton(InputSalto));
		*/

		#endif

		#if UNITY_ANDROID || UNITY_IOS	
		/*int TotalWidth = Screen.width;
		int HalfWidth = (int)(TotalWidth*0.5f);

		Touch firstTouch = Input.GetTouch(0);
		if(firstTouch.phase == TouchPhase.Stationary)
		{

			if(firstTouch.position.x > HalfWidth)
			{
				SendMessage(MessageHorizontal,1,SendMessageOptions.DontRequireReceiver);
			}
			else
			{
				SendMessage(MessageHorizontal,-1,SendMessageOptions.DontRequireReceiver);
			}
		}
		else
		{
			SendMessage(MessageHorizontal,0,SendMessageOptions.DontRequireReceiver);
		}*/

		int TotalWidth = Screen.width;
		int TotalHeight = Screen.height;


		int MoveWidth = (int)(TotalWidth*OffSetScreenMove.x);
		int MoveHeight = (int)(TotalHeight*OffSetScreenMove.y);


		int JumpWidth = (int)(TotalWidth*OffSetScreenJump.x);
		int JumpHeight = (int)(TotalWidth*OffSetScreenJump.y);


		if(Input.touches.Length > 0)
		{
			foreach(Touch data in Input.touches)
			{
				if(data.position.x < MoveWidth &&
				   data.position.y < MoveHeight &&
				   data.phase == TouchPhase.Began)
				{
					if(MoveTouchDet == false)
					{
						MoveTouchID = data.fingerId;
						MoveTouchDet = true;
					}
				}

				if(data.position.x > JumpWidth &&
				   data.position.y < JumpHeight)
				{
					if(data.fingerId != MoveTouchID)
					{
						if(data.tapCount > 0)
						{
							SendMessage(MessageSalto,true,SendMessageOptions.DontRequireReceiver);
						}
					}
				}
			}

			if(MoveTouchDet)
			{
				Touch MoveTouch = Input.GetTouch(MoveTouchID);
				if(MoveTouch.phase != TouchPhase.Ended || MoveTouch.phase == TouchPhase.Canceled)
				{
					if(MoveTouch.deltaPosition.x > OffsetTouchWalkRight)
					{
						SendMessage(MessageHorizontal,1,SendMessageOptions.DontRequireReceiver);
						if(MoveTouch.deltaTime > TimeTouchRunRight)
						{
							SendMessage(MessageCorrer,true,SendMessageOptions.DontRequireReceiver);
						}
						else
						{
							SendMessage(MessageCorrer,false,SendMessageOptions.DontRequireReceiver);
						}
					}
					else if(MoveTouch.deltaPosition.x < OffsetTouchWalkLeft*-1.0f)
					{
						SendMessage(MessageHorizontal,-1,SendMessageOptions.DontRequireReceiver);
						if(MoveTouch.deltaPosition.x < TimeTouchRunRight*-1.0f)
						{
							SendMessage(MessageCorrer,true,SendMessageOptions.DontRequireReceiver);
						}
						else
						{
							SendMessage(MessageCorrer,false,SendMessageOptions.DontRequireReceiver);
						}
					}
				}
				else
				{
					SendMessage(MessageHorizontal,0,SendMessageOptions.DontRequireReceiver);
					MoveTouchID = -1;
					MoveTouchDet = false;
				}
				
				if(MoveTouch.position.x > MoveWidth ||
				   MoveTouch.position.y > MoveHeight)
				{
					SendMessage(MessageHorizontal,0,SendMessageOptions.DontRequireReceiver);
					MoveTouchID = -1;
					MoveTouchDet = false;
				}
			}
			
		}
		
		#endif
	}
}
