﻿using UnityEngine;
using System.Collections;

public class AnimController : MonoBehaviour 
{
	public Animator MyAnim;
	public float VelCam;
	public float VelCorr;
	public float VelIdle;
	public float VelSalt;

	bool Corriendo = false;
	void Start () 
	{
		if(MyAnim == null)
		{
			MyAnim = gameObject.GetComponent<Animator>();
		}
		
		if(MyAnim == null)
		{
			Debug.LogError("NO EXISTE Animator");
		}
	}

	public void SetCaminando(bool doit, bool Correr = false)
	{
		MyAnim.SetBool("Caminando",doit);
		Corriendo = Correr;
	}

	void Update()
	{
		AnimatorStateInfo stateInfo = MyAnim.GetCurrentAnimatorStateInfo(0);
		
		if(stateInfo.nameHash == Animator.StringToHash("Base.Idle"))
		{
			MyAnim.speed = VelIdle;
		}
		else if(stateInfo.nameHash == Animator.StringToHash("Base.Caminando"))
		{
			if(Corriendo)
			{
				MyAnim.speed = VelCorr;
			}
			else
			{
				MyAnim.speed = VelCam;
			}
		}
		else if(stateInfo.nameHash == Animator.StringToHash("Base.Saltando"))
		{
			MyAnim.speed = VelSalt;
		}


	}

	public void SetSaltando(bool doit)
	{
		MyAnim.SetBool("Saltando",doit);
	}
}
