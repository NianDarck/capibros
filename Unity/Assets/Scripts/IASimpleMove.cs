﻿using UnityEngine;
using System.Collections;

public class IASimpleMove : MonoBehaviour 
{
	public bool CheckEdge = true;
	public bool CanJump = true;
	public float RadiusCheckPlayer;
	public float RadiusJumpCheck;
	public float EdgeMultiplier = 0.6f;
	public float DistanceJump = 10.0f;
	public float TimeToMove = 1.0f;
	float CurrentTimeToMove = 0.0f;
	bool SeePlayer = false;
	bool Moving = false;
	bool GoLeft = false;
	public string MessageHorizontal = "InputHorizontal";
	public string MessageSalto = "InputSalto";
	public string MessageCorrer = "InputCorrer";
	public string MessageAccion = "InputAccion";

	public Personaje MiPersonaje;

	// Update is called once per frame
	void FixedUpdate() 
	{
		/*Collider2D[] SphereColl = Physics2D.OverlapCircleAll(transform.position,1);
		foreach(Collider2D data in SphereColl)
		{
			Debug.Log(data.name);
		}*/
		LayerMask GetPlayer =(1 << LayerMask.NameToLayer("Player"));

		Collider2D PlayerCheck = Physics2D.OverlapCircle(transform.position,RadiusCheckPlayer,GetPlayer);

		if(PlayerCheck != null)
		{
			CurrentTimeToMove += Time.deltaTime;
			if(CurrentTimeToMove >= TimeToMove)
			{
				SeePlayer = true;
			}

			if(SeePlayer)
			{
				Moving = true;
				if(PlayerCheck.transform.position.x > transform.position.x)
				{
					GoLeft = false;
				}
				else if(PlayerCheck.transform.position.x < transform.position.x)
				{
					GoLeft = true;
				}

				if(CheckEdge && MiPersonaje.EnPiso)
				{
					SendMessage(MessageCorrer,false,SendMessageOptions.DontRequireReceiver);
					Vector2 DirPos = transform.position;
					int multiNextPiso = 1;
					if(GoLeft)
					{
						multiNextPiso = -1;
						DirPos.x -= transform.localScale.x*EdgeMultiplier;
					}
					else
					{
						multiNextPiso = 1;
						DirPos.x += transform.localScale.x*EdgeMultiplier;
					}

					LayerMask GetPiso = (1 << LayerMask.NameToLayer("Piso"));
					Ray2D RayPiso = new Ray2D(DirPos,transform.up*-1);
					RaycastHit2D HitPiso = Physics2D.Raycast(RayPiso.origin,RayPiso.direction,5,GetPiso);

					if(HitPiso.collider != null)
					{
						Moving = true;
					}
					else
					{
						Moving = false;
						if(CanJump)
						{
							Ray2D RayNextPiso = new Ray2D(DirPos,transform.right*multiNextPiso);
							RaycastHit2D[] PisoCheck = Physics2D.CircleCastAll(RayNextPiso.origin,RadiusJumpCheck,
							                                                   RayNextPiso.direction,DistanceJump,GetPiso);
							foreach(RaycastHit2D data in PisoCheck)
							{
								if(data.collider != null)
								{
									if(GoLeft)
									{
										if(data.collider.transform.position.x < DirPos.x)
										{
											SendMessage(MessageSalto,true,SendMessageOptions.DontRequireReceiver);
											Moving = true;
											break;
										}
									}
									else
									{
										if(data.collider.transform.position.x > DirPos.x)
										{
											SendMessage(MessageSalto,true,SendMessageOptions.DontRequireReceiver);
											Moving = true;
											break;
										}
									}
								}
							}
						}
					}
				}
				else
				{
					if(CanJump)
					{
						SendMessage(MessageCorrer,true,SendMessageOptions.DontRequireReceiver);
					}
				}

				if(Moving)
				{
					if(GoLeft)
					{
						SendMessage(MessageHorizontal,-1,SendMessageOptions.DontRequireReceiver);
					}
					else
					{
						SendMessage(MessageHorizontal,1,SendMessageOptions.DontRequireReceiver);
					}
				}
				else
				{
					SendMessage(MessageHorizontal,0,SendMessageOptions.DontRequireReceiver);
				}
			}
		}
		else
		{
			Moving = false;
			SeePlayer = false;
			if(CurrentTimeToMove > 0.0f)
			{
				CurrentTimeToMove -= Time.deltaTime;

				if(CheckEdge)
				{
					Vector2 DirPos = transform.position;
					if(GoLeft)
					{
						DirPos.x -= transform.localScale.x*0.8f;
					}
					else
					{
						DirPos.x += transform.localScale.x*0.8f;
					}
					
					LayerMask GetPiso = (1 << LayerMask.NameToLayer("Piso"));
					Ray2D RayPiso = new Ray2D(DirPos,transform.up*-1);
					RaycastHit2D HitPiso = Physics2D.Raycast(RayPiso.origin,RayPiso.direction,5,GetPiso);
					
					if(HitPiso.collider != null)
					{
						Moving = true;
					}
					else
					{
						Moving = false;
					}
				}

				if(Moving)
				{
					if(GoLeft)
					{
						SendMessage(MessageHorizontal,-1,SendMessageOptions.DontRequireReceiver);
					}
					else
					{
						SendMessage(MessageHorizontal,1,SendMessageOptions.DontRequireReceiver);
					}
				}
				else
				{
					SendMessage(MessageHorizontal,0,SendMessageOptions.DontRequireReceiver);
				}
			}
			else
			{
				SendMessage(MessageHorizontal,0,SendMessageOptions.DontRequireReceiver);
				CurrentTimeToMove = 0.0f;
			}
		}
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position,RadiusCheckPlayer);

		Gizmos.color = Color.yellow;
		Vector2 DirPos = transform.position;
		if(GoLeft)
		{
			DirPos.x -= transform.localScale.x*EdgeMultiplier;
		}
		else
		{
			DirPos.x += transform.localScale.x*EdgeMultiplier;
		}
		
		LayerMask GetPiso = (1 << LayerMask.NameToLayer("Piso"));
		Ray2D RayPiso = new Ray2D(DirPos,transform.up*-1);
		Gizmos.DrawRay(RayPiso.origin,RayPiso.direction);

		Gizmos.color = Color.blue;
		Vector3 FinalPosJumpCheck = new Vector3(DirPos.x,DirPos.y);
		if(GoLeft)
		{
			FinalPosJumpCheck.x -= DistanceJump;
		}
		else
		{
			FinalPosJumpCheck.x += DistanceJump;
		}
		Gizmos.DrawWireSphere(FinalPosJumpCheck,RadiusJumpCheck);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.collider.CompareTag("Player"))
		{
			GameManager.Instance.KillPlayer();
		}
	}
}
