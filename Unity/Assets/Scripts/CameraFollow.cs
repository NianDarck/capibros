using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	public Vector3 CamVel = Vector3.one;
	public bool FollowLeft = true;
	public bool FollowRight = true;
	public bool FollowUp = true;
	public bool FollowDown = true;

	public Vector2 MaxPos;
	public Vector2 MinPos;

	public Vector3 OffSetInit;
	bool ForceMoving = false;
	Vector2 PosMax
	{
		get
		{
			Vector2 tempPos = new Vector2(transform.position.x + MaxPos.x,transform.position.y + MaxPos.y);
			return tempPos;
		}
	}

	Vector2 PosMin
	{
		get
		{
			Vector2 tempPos = new Vector2(transform.position.x + MinPos.x,transform.position.y + MinPos.y);
			return tempPos;
		}
	}

	Camera MyCamera;
	GameObject SigueEsto;



	void Start()
	{
		MyCamera = GetComponent<Camera>();

		if(MyCamera == null)
		{
			Debug.LogError("There is no camera");
		}

		SigueEsto = GameManager.Instance.CurrentPersonaje.gameObject;
	}
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(SigueEsto == null || ForceMoving)
		{
			return;
		}

		if(SigueEsto.transform.position.x > PosMax.x)
		{
			transform.Translate(Vector3.right * (Time.deltaTime*CamVel.x));
		}
		else if(SigueEsto.transform.position.x < PosMin.x)
		{
			transform.Translate(Vector3.right * (Time.deltaTime*CamVel.x)*-1f);
		}


	}

	public void SetCamaraTo(Vector3 pos)
	{
		//ForceMoving = true;
		Vector3 finalPos = pos + OffSetInit;
		transform.position = finalPos;
		CancelInvoke("ResetMoving");
		Invoke("ResetMoving",1.0f);
	}

	void ResetMoving()
	{
		ForceMoving = false;
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Vector2 UpLeft = new Vector2(PosMin.x,PosMax.y);
		Vector2	DownLeft = new Vector2(PosMin.x,PosMin.y);
		Vector2 UpRight = new Vector2(PosMax.x,PosMax.y);
		Vector2 DownRight = new Vector2(PosMax.x,PosMin.y);

		Gizmos.DrawLine (UpLeft, UpRight);
		Gizmos.DrawLine (UpLeft, DownLeft);
		Gizmos.DrawLine (DownLeft, DownRight);
		Gizmos.DrawLine (UpRight, DownRight);
	}
}
