﻿using UnityEngine;
using System.Collections;

public class GameManager : Singleton<GameManager>
{
	public int InitVidas;
	public int VidasMax = 99;
	public int InitMonedas;
	public string InitNivel;

	public int CurrentVidas = 0;
	public int CurrentMonedas = 0;
	string CurrentNivel = string.Empty;
	public Personaje CurrentPersonaje;
	public CameraFollow CurrentCam;

	// Use this for initialization
	void Start() 
	{
		DontDestroyOnLoad(this.gameObject);
		MainChar tempMain = GameObject.FindObjectOfType<MainChar>();
		CurrentPersonaje = tempMain.GetComponent<Personaje>();
		CurrentCam = GameObject.FindObjectOfType<CameraFollow>();
		CurrentPersonaje.gameObject.SetActive(false);
	}

	public void Continue()
	{
		CurrentPersonaje.gameObject.SetActive(true);
		if(PlayerPrefs.HasKey("SecondTime"))
		{
			LoadData();
			UpdateNivel(CurrentNivel);
		}
		else
		{
			CurrentVidas = InitVidas;
			CurrentMonedas = InitMonedas;
			UpdateNivel(InitNivel);
			PlayerPrefs.SetInt("SecondTime",1);
			SaveData();
		}
	}

	public void StartNewGame()
	{
		CurrentPersonaje.gameObject.SetActive(true);
		CurrentVidas = InitVidas;
		CurrentMonedas = InitMonedas;
		UpdateNivel(InitNivel);
		PlayerPrefs.SetInt("SecondTime",1);
		SaveData();
	}

	public void SaveData()
	{
		PlayerPrefs.SetInt("CurrentVidas",CurrentVidas);
		PlayerPrefs.SetInt("CurrentMonedas",CurrentMonedas);
		PlayerPrefs.SetString("CurrentNivel",CurrentNivel);
		PlayerPrefs.Save();
	}

	public void LoadData()
	{
		CurrentVidas = PlayerPrefs.GetInt("CurrentVidas");
		CurrentMonedas = PlayerPrefs.GetInt("CurrentMonedas");
		CurrentNivel = PlayerPrefs.GetString("CurrentNivel");
	}

	public void UpdateVidas(int add)
	{
		CurrentVidas += add;
		if(CurrentVidas > VidasMax)
		{
			CurrentVidas = VidasMax;
		}
	}

	public void UpdateMonedas(int add)
	{
		CurrentMonedas += add;
	}

	public void KillPlayer()
	{
		UpdateVidas(-1);
		CurrentPersonaje.CambiarEstado(EnumEstado.Muerto);
		if(CurrentVidas < 0)
		{
			UpdateNivel(InitNivel);
			CurrentVidas = InitVidas;
		}
		else
		{
			SetCurrentLvlPos();
		}
	}

	public void UpdateNivel(string level)
	{
		LevelManager.Instance.SetAllLevel(CurrentNivel,false);
		CurrentNivel = level;
		PlayerPrefs.SetString("CurrentNivel",CurrentNivel);
		PlayerPrefs.Save();
		LevelManager.Instance.SetInitPoint(CurrentNivel,CurrentPersonaje.gameObject);
		CurrentCam.SetCamaraTo(CurrentPersonaje.transform.position);
	}

	public void SetCurrentLvlPos()
	{
		LevelManager.Instance.SetInitPoint(CurrentNivel,CurrentPersonaje.gameObject);
		CurrentCam.SetCamaraTo(CurrentPersonaje.transform.position);
	}

	public string GetCurrentNivel()
	{
		return CurrentNivel;
	}


	void LateUpdate()
	{
		if(!CurrentPersonaje.isActiveAndEnabled)
		{
			return;
		}

		if(LevelManager.Instance.SingleLevelData(CurrentNivel) != null)
		{
			if(LevelManager.Instance.SingleLevelData(CurrentNivel).EndingPos != null)
			{
				if(LevelManager.Instance.SingleLevelData(CurrentNivel).EndingPos.transform.position.x <
				   CurrentPersonaje.transform.position.x)
				{
					UpdateNivel(LevelManager.Instance.SingleLevelData(CurrentNivel).NextLvlID);
				}
			}
		}

	}
}
