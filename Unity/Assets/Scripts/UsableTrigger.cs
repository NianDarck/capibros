﻿using UnityEngine;
using System.Collections;

public class UsableTrigger : MonoBehaviour 
{
	public string IDAdd;

	void OnTriggerEnter(Collider coll)
	{
		Personaje tempPersonaje = coll.gameObject.GetComponent<Personaje>();
		if(tempPersonaje != null)
		{
			tempPersonaje.MiInventario.AddUsable(IDAdd);
			Destroy(gameObject);
		}
	}
}
