﻿                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              using UnityEngine;
using System.Collections;

/// <summary>
/// Tipo de Movmiento del personaje.
/// </summary>
public enum EnumMovimiento
{
	Quieto,
	Caminando,
	Correr
}

/// <summary>
/// Acciones que puedo hacer.
/// </summary>
public enum EnumAcciones
{
	Quieto,
	Saltar,
	LanzarFuego
}

/// <summary>
/// Que estado tengo.
/// </summary>
public enum EnumEstado
{
	Espera,
	Vivo,
	Muerto
}

/// <summary>
/// Direccion Actual.
/// </summary>
public enum EnumDireccion
{
	Izquierda,
	Derecha
}

/// <summary>
/// Podere Actual.
/// </summary>
public enum EnumPoderes
{
	Normal,
	Super,
	Flor,
	Estrella
}

public class Personaje : MonoBehaviour 
{
	public Inventario MiInventario;
	public AnimController MiAnim;
	public Transform VisualTransform;
	
	public EnumEstado MiEstado;
	public EnumMovimiento MiMovimiento;
	public EnumAcciones MiAccion;
	public EnumDireccion MiDireccion;
	public EnumDireccion PastMiDireccion;
	public EnumPoderes MisPoderesPasados;
	public EnumPoderes MisPoderes;
	
	public float DistanciaPiso = 0.8f;
	public float MulBFPiso = 0.5f;
	public float VelSalto = 0.3f;
	public float VelMulEnSalto = 0.5f;
	public float VelCaminado = 1.0f;
	public float VelCorriendo = 1.6f;
	bool CanMoveLeft = true;
	
	public float TiempoEstrella = 2.0f;
	float ActualTiempoEstrella = 0.0f;
	
	public float TiempoParaLanzarFuego = 0.5f;
	bool PuedoLanzarFuego;
	
	public Rigidbody MiRigidBody;
	public Rigidbody2D MiRigidBody2D;
	public CharacterController MiController;
	public float Gravedad;
	Vector3 CurrentVel = Vector3.zero;
	public bool EnPiso = false;
	bool Saltando = false;
	
	public float Vida;
	public float Mana;
	public float Stamina;


	public Vector3 instantVelocity;
	public Vector3 pos;

	// Use this for initialization
	void Start () 
	{
		PuedoLanzarFuego = true;
		MiEstado = EnumEstado.Vivo;
		MiMovimiento = EnumMovimiento.Quieto;
		MiDireccion = EnumDireccion.Derecha;
		PastMiDireccion = EnumDireccion.Derecha;
		MisPoderes = EnumPoderes.Normal;
		MiAccion = EnumAcciones.Quieto;
		EnPiso = false;
		Saltando = false;
		if(MiRigidBody == null)
		{
			MiRigidBody = gameObject.GetComponent<Rigidbody>();
		}
		
		if(MiRigidBody == null)
		{
			Debug.LogError("NO EXISTE RIGIDBODY");
		}

		if(MiRigidBody2D == null)
		{
			MiRigidBody2D = gameObject.GetComponent<Rigidbody2D>();
		}
		
		if(MiRigidBody2D == null)
		{
			Debug.LogError("NO EXISTE RIGIDBODY2D");
		}
		
		if(MiController == null)
		{
			MiController = gameObject.GetComponent<CharacterController>();
		}
		
		if(MiController == null)
		{
			Debug.LogError("NO EXISTE CharacterController");
		}

		if(MiAnim == null)
		{
			MiAnim = gameObject.GetComponent<AnimController>();
		}
		
		if(MiAnim == null)
		{
			Debug.LogError("NO EXISTE ANIMATOR");
		}
	}
	
	public void CambiarEstado(EnumEstado CambioDeEstado)
	{
		MiEstado = CambioDeEstado;
	}
	
	public void CambiarDireccion(EnumDireccion CambioDeEstado)
	{
		if(EnPiso)
		{
			PastMiDireccion = MiDireccion;
		}
		MiDireccion = CambioDeEstado;
	}
	
	bool DifDireccion()
	{
		if(PastMiDireccion != MiDireccion)
		{
			return true;
		}
		return false;
	}
	
	public void CambiarAccion(EnumAcciones CambioDeEstado)
	{
		MiAccion = CambioDeEstado;
	}
	
	public void CambiarMovimiento(EnumMovimiento CambioDeEstado)
	{
		MiMovimiento = CambioDeEstado;
	}
	
	public void InputHorizontal(float HorizontalData)
	{
		if(HorizontalData > 0)
		{
			CambiarDireccion(EnumDireccion.Derecha);
			if(MiMovimiento != EnumMovimiento.Correr)
			{
				CambiarMovimiento(EnumMovimiento.Caminando);
			}
		}
		else if(HorizontalData < 0)
		{
			CambiarDireccion(EnumDireccion.Izquierda);
			if(MiMovimiento != EnumMovimiento.Correr)
			{
				CambiarMovimiento(EnumMovimiento.Caminando);
			}
		}
		else
		{
			CambiarMovimiento(EnumMovimiento.Quieto);
		}
	}
	
	public void InputSalto(bool Doit)
	{
		if(Doit)
		{
			CambiarAccion(EnumAcciones.Saltar);
		}
	}
	
	public void InputAccion(bool Doit)
	{
		if(Doit)
		{
			if(MisPoderes == EnumPoderes.Flor)
			{
				CambiarAccion(EnumAcciones.LanzarFuego);	
			}
		}
	}
	
	public void InputCorrer(bool Doit)
	{
		if(MiMovimiento == EnumMovimiento.Quieto)
		{
			return;
		}
		
		if(Doit)
		{
			CambiarMovimiento(EnumMovimiento.Correr);
		}
		else
		{
			MiMovimiento = EnumMovimiento.Caminando;
		}
	}
	
	void Direccion()
	{
		switch(MiDireccion) 
		{
		case EnumDireccion.Izquierda:
		{
			//Debug.Log("Vista a la Izquierda");
			if(VisualTransform != null)
			{
				if(VisualTransform.localScale.x > 0)
				{
					VisualTransform.localScale = new Vector3(VisualTransform.localScale.x*-1,
					                                         VisualTransform.localScale.y,
					                                         VisualTransform.localScale.z*-1);
				}
			}
		}
			break;
		case EnumDireccion.Derecha:
		{
			//Debug.Log("Vista a la Derecha");
			if(VisualTransform != null)
			{
				if(VisualTransform.localScale.x < 0)
				{
					VisualTransform.localScale = new Vector3(VisualTransform.localScale.x*-1,
					                                         VisualTransform.localScale.y,
					                                         VisualTransform.localScale.z*-1);
				}
			}
		}
			break;
		}
	}
	
	void Acciones()
	{
		switch(MiAccion) 
		{
		case EnumAcciones.Quieto:
		{
			//Debug.Log("Esperando Accion");
		}
			break;
		case EnumAcciones.Saltar:
		{
			//Debug.Log("Hacer Salto");
			if(EnPiso)
			{
				Saltando = true;
				EnPiso = false;
				CurrentVel.y = 0.0f;
				CurrentVel.y = VelSalto;
			}
			MiAccion = EnumAcciones.Quieto;
		}
			break;
		case EnumAcciones.LanzarFuego:
		{
			if(PuedoLanzarFuego)
			{
				CancelInvoke("ChecarLanzarFuego");
				Invoke("ChecarLanzarFuego",TiempoParaLanzarFuego);
				PuedoLanzarFuego = false;
				//Debug.Log("Hacer LanzarFuego");
			}
			MiAccion = EnumAcciones.Quieto;
		}
			break;
		}
	}
	
	void ChecarLanzarFuego()
	{
		PuedoLanzarFuego = true;
	}
	
	public void SetCanMoveLeft(bool doit)
	{
		CanMoveLeft = doit;
	}
	
	void Movimiento()
	{
		switch(MiMovimiento)
		{
		case EnumMovimiento.Quieto:
		{	
			CurrentVel.x = 0;
			MiAnim.SetCaminando(false);
		}
			break;
		case EnumMovimiento.Caminando:
		{
			MiAnim.SetCaminando(true);
			float finalVelCaminando = (VelCaminado)*Time.deltaTime;
			if(!EnPiso && DifDireccion())
			{
				finalVelCaminando*=VelMulEnSalto;
			}
			
			if(MiDireccion == EnumDireccion.Derecha)
			{
				CurrentVel.x = finalVelCaminando;
			}
			else if(MiDireccion == EnumDireccion.Izquierda)
			{
				if(!CanMoveLeft)
				{
					CurrentVel.x = 0.0f;
				}
				else
				{
					finalVelCaminando*=-1.0f;
					CurrentVel.x = finalVelCaminando;
				}
			}
		}
			break;
		case EnumMovimiento.Correr:
		{
			MiAnim.SetCaminando(true,true);
			float finalVelCorriendo = (VelCorriendo)*Time.deltaTime;
			if(Saltando  && DifDireccion())
			{
				finalVelCorriendo*=VelMulEnSalto;
			}
			
			if(MiDireccion == EnumDireccion.Derecha)
			{
				CurrentVel.x = finalVelCorriendo;
			}
			else if(MiDireccion == EnumDireccion.Izquierda)
			{
				if(!CanMoveLeft)
				{
					CurrentVel.x = 0.0f;
				}
				else
				{
					finalVelCorriendo*=-1.0f;
					CurrentVel.x = finalVelCorriendo;
				}
			}
		}
			break;
		}
	}
	
	void Poderes()
	{
		switch(MisPoderes) 
		{
		case EnumPoderes.Normal:
		{
			//Debug.Log("Poder: Normal");
		}
			break;
		case EnumPoderes.Super:
		{
			//Debug.Log("Poder: Super");
		}
			break;
		case EnumPoderes.Flor:
		{
			//Debug.Log("Poder: Flor");
		}
			break;
		case EnumPoderes.Estrella:
		{
			//Debug.Log("Poder: Estrella");
			ActualTiempoEstrella += Time.deltaTime;
			if(ActualTiempoEstrella > TiempoEstrella)
			{
				MisPoderes = MisPoderesPasados;
				ActualTiempoEstrella = 0.0f;
			}
		}
			break;
		}
	}
	
	void ChecarPiso()
	{
		if(EnPiso)
		{
			Saltando = false;
		}
		
		if(MiRigidBody)
		{
			Ray tempRayC = new Ray(transform.position,transform.transform.up*-1);
			
			Vector3 BackPos = transform.position;
			BackPos.x -= transform.localScale.x*0.5f;
			Ray tempRayB = new Ray(BackPos,transform.transform.up*-1);
			
			Vector3 FrontPos = transform.position;
			FrontPos.x += transform.localScale.x*0.5f;
			Ray tempRayF = new Ray(FrontPos,transform.transform.up*-1);
			
			RaycastHit temphit = new RaycastHit();
			if(Physics.Raycast(tempRayC,out temphit,DistanciaPiso))
			{
				EnPiso = true;
			}
			else if(Physics.Raycast(tempRayB,out temphit,DistanciaPiso))
			{
				EnPiso = true;
			}
			else if(Physics.Raycast(tempRayF,out temphit,DistanciaPiso))
			{
				EnPiso = true;
			}
			else
			{
				EnPiso = false;
			}
		}
		else if(MiRigidBody2D)
		{
			Ray2D tempRayC = new Ray2D(transform.position,transform.transform.up*-1);
			
			Vector3 BackPos = transform.position;
			BackPos.x -= transform.localScale.x*MulBFPiso;
			Ray2D tempRayB = new Ray2D(BackPos,transform.transform.up*-1);
			
			Vector3 FrontPos = transform.position;
			FrontPos.x += transform.localScale.x*MulBFPiso;
			Ray2D tempRayF = new Ray2D(FrontPos,transform.transform.up*-1);
			
			RaycastHit2D temphitC = Physics2D.Raycast(tempRayC.origin,tempRayC.direction,DistanciaPiso);
			RaycastHit2D temphitB = Physics2D.Raycast(tempRayB.origin,tempRayB.direction,DistanciaPiso);
			RaycastHit2D temphitF = Physics2D.Raycast(tempRayF.origin,tempRayF.direction,DistanciaPiso);

			if(temphitC.collider != null)
			{
				EnPiso = true;
			}
			else if(temphitB.collider != null)
			{
				EnPiso = true;
			}
			else if(temphitF.collider != null)
			{
				EnPiso = true;
			}
			else
			{
				EnPiso = false;
			}
		}
		else if(MiController)
		{
			EnPiso = MiController.isGrounded;
		}
	}
	
	void ChecarGravedad()
	{
		if(!EnPiso)
		{
			CurrentVel.y += Gravedad*Time.deltaTime;
		}
		else
		{
			if(!Saltando)
			{
				CurrentVel.y = 0;
			}
		}
	}

	void Update()
	{
		ChecarPiso();
	}

	void LateUpdate()
	{
		ChecarGravedad();
		instantVelocity = transform.position - pos;
	}
	
	//Update se llama cada frame
	void FixedUpdate() 
	{
		switch(MiEstado)
		{
		case EnumEstado.Espera:
		{
			MiEstado = EnumEstado.Vivo;
		}
			break;
		case EnumEstado.Vivo:
		{
			pos = transform.position;
			MiAnim.SetSaltando(Saltando);
			Direccion();
			Movimiento();
			Acciones();
			Poderes();
			float tempFinalVelX = CurrentVel.x;
			float tempFinalVelY = CurrentVel.y;
			Vector3 finalVel = new Vector3(tempFinalVelX,tempFinalVelY,0.0f);
			
			if(MiRigidBody)
			{
				MiRigidBody.velocity = finalVel;
			}

			if(MiRigidBody2D)
			{
				MiRigidBody2D.velocity = new Vector2(finalVel.x,finalVel.y);
			}

			if(MiController)
			{
				MiController.Move(finalVel);
			}



		}
			break;
		case EnumEstado.Muerto:
		{
			//Debug.Log("Muerto");				
			MiEstado = EnumEstado.Espera;
		}
			break;
		}
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;

		Vector3 CenterPos = transform.position;
		Vector3 EndCenterPos = CenterPos;
		EndCenterPos.y -= DistanciaPiso;
		
		Vector3 BackPos = transform.position;
		BackPos.x -= transform.localScale.x*MulBFPiso;
		Vector3 EndBackPos = BackPos;
		EndBackPos.y -= DistanciaPiso;
		
		Vector3 FrontPos = transform.position;
		FrontPos.x += transform.localScale.x*MulBFPiso;
		Vector3 EndFrontPos = FrontPos;
		EndFrontPos.y -= DistanciaPiso;

		Gizmos.DrawLine(CenterPos,EndCenterPos);
		Gizmos.DrawLine(BackPos,EndBackPos);
		Gizmos.DrawLine(FrontPos,EndFrontPos);
	}
}
