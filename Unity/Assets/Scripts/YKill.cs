﻿using UnityEngine;
using System.Collections;

public class YKill : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			GameManager.Instance.KillPlayer();
		}
	}
}
