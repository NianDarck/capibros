﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SimpleAudio
{
	public string ID;
	public AudioClip clip;
	public bool loop;
	public bool Music;
}

public class AudioManager : Singleton<AudioManager> 
{
	public AudioSource MusicSource;
	public List<SimpleAudio> AllAudios;
	Dictionary<string,SimpleAudio> DicAudios = new Dictionary<string, SimpleAudio>();
	Dictionary<string,AudioSource> CurrentSources = new Dictionary<string, AudioSource>();
	GameObject AllAudioSources;
	public string InitAudio;

	void Start()
	{
		AllAudioSources = new GameObject("AudioSoruces");
		AllAudioSources.transform.parent = transform;
		AllAudioSources.transform.localPosition = Vector3.zero;

		foreach(SimpleAudio data in AllAudios)
		{
			if(!DicAudios.ContainsKey(data.ID))
			{
				DicAudios.Add(data.ID,data);
			}
		}

		ActiveAudio(InitAudio);
	}

	public void ActiveAudio(string ID,bool doit = true,Transform NewParent = null)
	{
		if(DicAudios.ContainsKey(ID))
		{
			SimpleAudio TempAudio = DicAudios[ID];
			if(TempAudio.Music)
			{
				MusicSource.Stop();
				if(doit)
				{
					MusicSource.loop = TempAudio.loop;
					MusicSource.clip = TempAudio.clip;
					MusicSource.Play();
				}
			}
			else
			{
				if(CurrentSources.ContainsKey(ID))
				{
					AudioSource TempAudioSurce = CurrentSources[ID];
					if(!doit)
					{
						TempAudioSurce.Stop();
					}
					else
					{
						TempAudioSurce.clip = TempAudio.clip;
						TempAudioSurce.loop = TempAudio.loop;
						if(TempAudioSurce.loop)
						{
							if(!TempAudioSurce.isPlaying)
							{
								TempAudioSurce.Play();
							}
						}
						else
						{
							TempAudioSurce.Stop();
							TempAudioSurce.Play();
						}
					}
				}
				else
				{
					if(!doit)
					{
						return;
					}

					GameObject gameAS = new GameObject(ID);
					AudioSource TempAudioSurce = gameAS.AddComponent<AudioSource>();
					gameAS.transform.parent = AllAudioSources.transform;
					if(NewParent != null)
					{
						TempAudioSurce.transform.parent = NewParent;
					}
					TempAudioSurce.transform.localPosition = Vector3.zero;

					if(doit)
					{
						TempAudioSurce.clip = TempAudio.clip;
						TempAudioSurce.loop = TempAudio.loop;
						TempAudioSurce.Play();
					}
					CurrentSources.Add(ID,TempAudioSurce);
				}
			}
		}
	}
}
