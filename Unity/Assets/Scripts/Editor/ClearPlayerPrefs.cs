﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ClearPlayerPrefs : MonoBehaviour 
{

	[MenuItem ("Window/Delete PlayerPrefs")]
	static void DoSomething () 
	{
		PlayerPrefs.DeleteAll();
	}
}
