﻿using UnityEngine;
using System.Collections;

public class SteeringBehaviours : MonoBehaviour 
{
	public Transform target;
	Personaje VelocityTarget;
	public float moveSpeed = 6.0f;
	public float rotationSpeed = 1.0f;
	
	public int minDistance = 5;
	public int safeDistance = 60;
	public int iterationAhead  = 30;
	public enum AIState 
	{
		Idle,
		Seek,
		Flee,
		Arrive,
		Pursuit,
		Evade
	}
	public AIState currentState;


	void Start()
	{
		if(target != null)
		{
			VelocityTarget = target.GetComponent<Personaje>();
		}
	}

	void FixedUpdate() 
	{
		switch(currentState)
		{
		case AIState.Idle:
			break;
		case AIState.Seek:
			Seek();
			break;
		case AIState.Flee:
			Flee();
			break;
		case AIState.Arrive:
			Arrive();
			break;
		case AIState.Pursuit:
			Pursuit();
			break;
		case AIState.Evade:
			Evade();
			break;
		}
	}


	void Seek ()
	{
		if(target == null || VelocityTarget == null)
		{
			return;
		}
		Vector3 direction = target.position - transform.position;
		direction.z = 0;
		direction.y = 0;
		
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
		
		if(direction.magnitude > minDistance)
		{
			Vector3 moveVector = direction.normalized * moveSpeed * Time.deltaTime;
			
			transform.position += moveVector;
			
		}
	}
	
	void Flee ()
	{
		if(target == null || VelocityTarget == null)
		{
			return;
		}

		Vector3 direction = transform.position - target.position;
		direction.z = 0;
		direction.y = 0;
		
		if(direction.magnitude < safeDistance)
		{
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
			Vector3 moveVector = direction.normalized * moveSpeed * Time.deltaTime;
			transform.position += moveVector;
		}
	}
	
	void Arrive ()
	{
		if(target == null || VelocityTarget == null)
		{
			return;
		}

		Vector3 direction = target.position - transform.position;
		direction.z = 0;
		direction.y = 0;
		
		float distance = direction.magnitude;
		
		float decelerationFactor = distance / 5.0f;
		
		float speed = moveSpeed * decelerationFactor;
		
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
		
		Vector3 moveVector = direction.normalized * Time.deltaTime * speed;
		transform.position += moveVector;
	}
	
	void Pursuit ()
	{
		if(target == null || VelocityTarget == null)
		{
			return;
		}

		Vector3 targetSpeed = VelocityTarget.instantVelocity;
		Debug.Log(targetSpeed);
		Vector3 targetFuturePosition = target.transform.position + (targetSpeed * iterationAhead);
		
		Vector3 direction = targetFuturePosition - transform.position;
		direction.z = 0;
		direction.y = 0;
		
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
		
		if(direction.magnitude > minDistance)
		{
			
			Vector3 moveVector = direction.normalized * moveSpeed * Time.deltaTime;
			
			transform.position += moveVector;
			
		}
	}
	
	void Evade ()
	{
		if(target == null || VelocityTarget == null)
		{
			return;
		}
		
		Vector3 targetSpeed = VelocityTarget.instantVelocity;
		
		Vector3 targetFuturePosition = target.position + (targetSpeed * iterationAhead);
		
		Vector3 direction = transform.position - targetFuturePosition;
		direction.z = 0;
		direction.y = 0;
		
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
		
		if(direction.magnitude < safeDistance)
		{
			
			Vector3 moveVector = direction.normalized * moveSpeed * Time.deltaTime;
			
			transform.position += moveVector;
			
		}
	}
}
